.. _ref:

################
Reference guides
################

The following reference guides are the technical descriptions of how to operate Attune.

.. toctree::
    :titlesonly:

    architecture
    workspace_design
    workspace_plan
    workspace_run
    scripts_referencing_parameters
    git_ftp_ref
