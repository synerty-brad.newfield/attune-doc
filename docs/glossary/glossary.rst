.. _glossary:

########
Glossary
########


**Blueprints**
A Blueprint is the orchestration of Steps.

**Jobs**
Jobs are created in the Plan Portal, pairing Values with the Parameters that are configured in the Blueprint. Running a job will perform the tasks configured in the Blueprint using the Values configured in the Parameters.

**Parameters**
Steps contains Parameters for credentials, node details, text, and more.

**Steps**
A Step contains the connection details and action(s) to be performed.

**Values**
Values are substituted into a Job Parameters when a Job is run.
