.. _attune_best_practices_design:

#############################
Designing Automation Projects
#############################

Projects
========

**Creating Projects**

Ideally, you aim to initiate a Project with a defined purpose, crafting it as 
a modular building block for seamless integration into end-to-end automated 
workflows.

Use a name that describes the purpose of the Project. If the Project will be 
shared publicly, consider Search Engine Optimisation (SEO) when naming the 
Project. Attune generates the files for 
`GitHub Pages <https://pages.github.com/>`_ 
to generate a website for your Project.

The following examples are Projects created to be modular building blocks:

* `Create Windows ISO with autounattend built on Windows <https://github.com/Attune-Automation/Create-Windows-ISO-with-autounattend-built-on-Windows>`_
* `Create Windows ISO with autounattend built on macOS or Linux <https://github.com/Attune-Automation/Create-Windows-ISO-with-autounattend-built-on-macOS-or-Linux>`_
* `Create Red Hat Enterprise Linux RHEL with kickstart built on macOS or Linux <https://github.com/Attune-Automation/Create-Red-Hat-Enterprise-Linux-RHEL-ISO-with-kickstart-built-on-macOS-or-Linux>`_
* `Intel NUC Drivers <https://github.com/Attune-Automation/Intel-NUC-Drivers>`_
* `MSI Drivers <https://github.com/Attune-Automation/MSI-Drivers>`_
* `PiKVM APIs from Windows <https://github.com/Attune-Automation/PiKVM-APIs-on-Windows>`_
* `PiKVM APIs from macOS or Linux <https://github.com/Attune-Automation/PiKVM-APIs-on-macOS-or-Linux>`_
* `VMWare ESXi APIs from macOS or Linux <https://github.com/Attune-Automation/VMWare-ESXi-APIs-from-macOS-or-Linux>`_
* `oVirt APIs from macOS or Linux <https://github.com/Attune-Automation/ovirt-APIs-from-macOS-or-Linux>`_
* `PiKVM Windows Installer from a Windows Machine <https://github.com/Attune-Automation/PiKVM-Windows-Installer-from-a-Windows-Machine>`_

The following examples link Blueprints from the above Projects to automate an 
end-to-end process:

* `PiKVM Windows Installer from a Windows Machine <https://github.com/Attune-Automation/PiKVM-Windows-Installer-from-a-Windows-Machine>`_
* `VMware ESXi Red Hat Enterprise RHEL Installer from a macOS or Linux Machine <https://github.com/Attune-Automation/VMware-ESXi-Red-Hat-Enterprise-Linux-RHEL-Installer-from-a-macOS-or-Linux-Machine>`_

This promotes re-usability and makes it easier to update and maintain
specific pieces of functionality.

**Project Descriptions**

Use the Project description to describe the purpose of the Project. A
quality Project description encourages collaboration, maintainability, and 
knowledge transfer.

**Project Management**

You can version control your Projects, ensure you frequently commit your 
changes. Publish and push your commits to Git repositories. Leverage 
publicly shared Projects such as 
`Attune Automation on GitHub <https://github.com/Attune-Automation>`_.

Blueprints
==========

**Creating Blueprints**

Blueprints complete a required process. Make best effort to use
Parameters so that you minimise the number of Blueprints to maintain. 
Use a name that describes the process being performed.

**Blueprint Descriptions**

Use the Blueprint description to describe the process performed by the 
Blueprint. A quality Blueprint description encourages collaboration, 
maintainability, and knowledge transfer.

Steps
=====

Break down your script into small, discrete steps, each dedicated to 
performing a specific action. Additionally, ensure that your scripts 
are designed to be idempotent, meaning they can be run multiple 
times without causing unexpected side effects. This approach not only 
enhances the clarity of your code but also simplifies the debugging 
process, making it more efficient and less error-prone.

Parameters
==========

Using Parameters in Steps and Files greatly increases the flexibility 
of the Blueprints. Ensure that names are intuitive and that comments 
clearly describe the expected value to help users when populating the 
Plans.
