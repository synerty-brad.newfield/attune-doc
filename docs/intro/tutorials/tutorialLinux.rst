.. _tutorial_linux:

###############################################
Creating your first Linux or macOS Project
###############################################

Let's learn by example.

Throughout this tutorial, we'll walk you through the creation and running of an automated process.

It'll consist of three parts:

1. Design
2. Plan
3. Run

We'll assume you have Attune installed already. This tutorial is written for Attune v23.7.3 targeting a Linux
or macOS machine.

.. note::
    If you're having trouble going through this tutorial, please head over to the :ref:`Getting Help<getting_help>`
    section.

----

Part 1 - Design
===============

Create a project
----------------

In the Attune application, navigate to the Design workspace and create a new project.

1. Open the **Project** dropdown
2. Select **New Project** at the bottom of the list
3. Choose **Create blank project**
4. Enter a **Project Name** such as: ``Linux Tutorial``
5. Select **Create**
6. Select **Switch Project**

This will create and select the new Project. Verify that the new project is selected in the Project dropdown.

----

Create parameters
-----------------

Open the **Parameters** drawer in the Design workspace and create the parameters listed in the table below.

.. list-table:: Parameters
    :widths: 50 50
    :header-rows: 1

    * - Type
      - Name
    * - Linux/Unix Node
      - ``Tutorial Node``
    * - Linux/Unix Credential
      - ``Tutorial User``
    * - Basic Node
      - ``Tcp Ping Node``
    * - Text
      - ``Continue Tutorial``

Close the **Parameters** drawer when you're finished.

----

Create files
------------

Open the **Files** drawer in the Design workspace and create the two files as per below.

Firstly, create a version controlled file:

1. Select **Create File Archive**
2. Set the **Type**: ``Version Controlled Files``
3. Enter the **Name**: ``Tutorial Config``
4. Select **Create**

Create a file within the file you created:

1. Select the root folder ``./``
2. Select **Create File**
3. Enter the **Name**: ``config.cfg``
4. Select **Create**
5. Select the ``config.cfg`` you created
6. Enter the code below

.. code-block::

    Tutorial Host: {node.hostname}
    Tutorial User: {credentials.user}

7. Select **Save**

Secondly, create a large file archive:

1. Select **Create File Archive**
2. Set the **Type**: ``Version Controlled Files``
3. Enter the **Name**: ``Tutorial Large File``
4. Select **Create**
5. Drag a file and drop it on the **Replace Contents** section.

Close the **Files** drawer when you're finished.

----

Create a blueprint
------------------

Create a new blueprint.

1. Select **Create Blueprint**
2. Set the Blueprint **Name**: ``Linux Tutorial Blueprint``
3. Select **Create**

----

Create the steps
----------------

Execute Linux script
********************

Select the blueprint you created. On the step select the **Create Step** Option.

.. list-table:: Execute Linux Script
    :widths: 50 150
    :header-rows: 1

    * - Type
      - ``Execute Linux Script``
    * - Name
      - ``Linux Tutorial execute linux step``
    * - Target Node
      - ``Tutorial Node``
    * - Credential
      - ``Tutorial User``

.. note::
    The following script will intentionally fail to later demonstrate fixing a running job.

**Script**:

.. code-block::

    echo "The operation failed."

    exit 1


Select **Save**

.. note::
    At this point you can skip to :ref:`Part 2<tutorial_linux_part_two>`.

Execute Linux script with responses
***********************************

Select the previous step you created. On the step select the **Create Step** Option.

.. list-table:: Execute Linux Script With Responses
    :widths: 50 150
    :stub-columns: 1

    * - Type
      - ``Execute Linux Script With Responses``
    * - Name
      - ``Linux Tutorial execute linux step with responses``
    * - Target Node
      - ``Tutorial Node``
    * - Credential
      - ``Tutorial User``
    * - Prompt Response
      - ``Continue?==={continueTutorial}``

**Script**:

.. code-block::

    echo "Continue?"
    read response

    lowercase_string=$(echo "$response" | tr '[:upper:]' '[:lower:]')

    if [ "$lowercase_string" == "yes" ] || [ "$lowercase_string" == "y" ]; then
        echo "You responded: {continueTutorial}!"
    else
        echo "You responded: {continueTutorial}! This step will stop!"
        exit 1
    fi

Select **Save**

Tcp ping
********

Select the previous step you created. On the step select the **Create Step** Option.

.. list-table:: Tcp Ping
    :widths: 50 150
    :stub-columns: 1

    * - Type
      - ``Tcp Ping``
    * - Name
      - ``Linux Tutorial tcp ping``
    * - Target Node
      - ``Tcp Ping Node``

1. Select **Save**
2. Drag the step to the beginning of the Blueprint

Group step
**********

Select the last step in the Blueprint. On the step select the **Create Step** Option.

.. list-table:: Group Step
    :widths: 50 150
    :stub-columns: 1

    * - Type
      - ``Group Step``
    * - Name
      - ``Linux Tutorial group step``

Select **Save**

Push files
**********

Select the group step in the Blueprint. On the step select the **Create Step** Option. Select **> Under** to create
the step in the group step.

.. list-table:: Push Files
    :widths: 50 150
    :stub-columns: 1

    * - Type
      - ``Push Files``
    * - Name
      - ``Linux Tutorial push files``
    * - Target Node
      - ``Tutorial Node``
    * - Credential
      - ``Tutorial User``
    * - Files
      - ``Tutorial Large File``
    * - Remote Path
      - ``~/attuneTutorial/``

Select **Save**

Push compiled files
*******************

Select the previous step you created. On the step select the **Create Step** Option.

.. list-table:: Push Compiled Files
    :widths: 50 150
    :stub-columns: 1

    * - Type
      - ``Push Compiled Files``
    * - Name
      - ``Linux Tutorial push compiled files``
    * - Target Node
      - ``Tutorial Node``
    * - Credential
      - ``Tutorial User``
    * - Files
      - ``Tutorial Config``
    * - Remote Path
      - ``~/attuneTutorial/``

Check the **config.cfg** file.

.. list-table:: config.cfg
    :widths: 70 70 70
    :header-rows: 1

    * - Mako Parameter
      - Type
      - Attune Parameter
    * - ``node``
      - ``Linux/Unix Node``
      - ``Tutorial Node``
    * - ``credentials``
      - ``Linux/Unix Credential``
      - ``Tutorial User``

1. Select **Test Dynamic File Generation** and verify there are no errors in the Mako script
2. Select **Save**

Cleanup the files
*****************

Select the previous step you created. On the step select the **Create Step** Option.

.. list-table:: Cleanup Files
    :widths: 50 150
    :header-rows: 1

    * - Type
      - ``Execute Linux Script``
    * - Name
      - ``Linux Tutorial cleanup files``
    * - Target Node
      - ``Tutorial Node``
    * - Credential
      - ``Tutorial User``

**Script**:

.. code-block::

    directory="~/attuneTutorial/"

    if [ -d $directory ]; then
        cd $directory
        echo "Listing all files in $directory:"
        ls -ltrh
        echo "Displaying the contents of config.cfg:"
        cat config.cfg
        echo "Moving out of $directory to prepare for deletion."
        cd ~
        echo "Removing the directory $directory and its contents."
        rm -rf $directory
    else
        echo "$directory doesn't exist!"
    fi

Select **Save**

Link existing step
******************

Select the group step in the Blueprint. On the step select the **Link Existing Step** Option. Select **V After** to
link a step after the group step.

1. Set **Step**: ``Linux Tutorial execute linux step``
2. Select **Link**

Commit your project changes
***************************

1. Select **Commit**
2. Set the **Commit Message**: ``Created Linux Tutorial Blueprint``
3. Select **Commit**

Congratulations, you have created your Blueprint.

----

.. _tutorial_linux_part_two:

Part 2 - Plan
=============

In the Attune application, navigate to the Plan workspace.

Create values
-------------

Open the **Values** drawer in the Design workspace and create the three values required for the job.

Linux/Unix Node
***************

1. Select **Create Value**
2. Populate the form

.. list-table:: Linux/Unix Node
    :widths: 50 150
    :stub-columns: 1

    * - Type
      - Linux/Unix Node
    * - Name
      - {name of your device}
    * - IP Address
      - {device IP address}
    * - Hostname
      - {hostname}
    * - Domain Name
      - {domain name}

3. Select **Create**

Linux/Unix Credential
*********************

1. Select **Create Value**
2. Populate the form

.. list-table:: Linux/Unix Credential
    :widths: 50 150
    :stub-columns: 1

    * - Type
      - Linux/Unix Credential
    * - Name
      - {name of your user}
    * - User
      - {username}

3. Select **Create**
4. Find the value and update the **Password**

Basic Node
**********

1. Select **Create Value**
2. Populate the form

.. list-table:: Linux/Unix Node
    :widths: 50 150
    :stub-columns: 1

    * - Type
      - Basic Node
    * - Name
      - ``search.brave.com``
    * - IP Address
      - ``search.brave.com``
    * - Hostname
      - ``search``
    * - Domain Name
      - ``brave.com``

3. Select **Create**

Text
****

1. Select **Create Value**
2. Populate the form

.. list-table:: Text
    :widths: 50 50
    :stub-columns: 1

    * - Type
      - Text
    * - Name
      - ``yes``
    * - Variable
      - ``yes``

3. Select **Create**

Close the **Values** drawer when you're finished.

----

Create the tree
---------------

Create the tree of your environment.

1. Select **Create Tree**
2. Set the **Name**: {environment name}
3. Select **Create**
4. Expand the dropdown on the first node in your tree and select **Create Node**
5. Set the **Name**: {device hostname}
6. Select **Create**

----

Create the plan
---------------

Create the plan for your tutorial job.

1. Expand the dropdown on the device node and select **Create Plan**
2. Set the **Name**: ``{hostname} Linux Tutorial Job``
3. Select **Create**
4. Select the plan you created
5. **Add Blueprint**
6. Select **Save**
7. Populate the Values

.. list-table:: Plan Inputs
    :widths: 50 150
    :header-rows: 1

    * - Parameter
      - Value
    * - Tutorial Node
      - select the device
    * - Tutorial User
      - select the credentials
    * - Tcp Ping Node
      - select the ping node ``search.brave.com``
    * - Continue Tutorial
      - select ``yes``

8. Select **Save**

Congratulations, you have created your Plan.

----

.. _tutorial_linux_part_three:

Part 3 - Run
============

In the Attune application, navigate to the Run workspace.

1. Search for and select you Job
2. Select **Reset Job**
3. Select play

----

Fix the error
-------------

Fix the script on the failed step and resume the job.

1. Select the failed step **Linux Tutorial execute linux step**
2. Select **Edit Step**
3. Replace the **Script** with the following

.. code-block::

    echo "This step works!"


4. Select **Save**
5. Select **Run only this step**
6. Verify the step run successfully
7. Select **Run job from select step**

----

Reset the Job and archive the current logs
------------------------------------------

Review the logs for each steps then archive.

1. Inspect each step
2. Select **Reset the job and archive the current logs**
3. Select the **History** tab
4. Inspect the most recent log

----

Tutorial completed
==================

Congratulations, you have completed the tutorial.
