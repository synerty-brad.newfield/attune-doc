.. _intro:

###############
Getting Started
###############

New to Attune? Or to scripting in general? Well, you came to the right place: read this material to quickly get up
and running.

Explore Attune with our brief 7-minute demonstration video:

.. raw:: html

    <embed>
        <iframe height="500pt" width="700pt"
        src="https://www.youtube-nocookie.com/embed/xzs0xGPar78"
        title="YouTube video player" frameborder="0" allow="accelerometer;
        autoplay; clipboard-write; encrypted-media; gyroscope;
        picture-in-picture" allowfullscreen></iframe>
    </embed>

:Video: Demonstration of creating an automated process in Attune.

If you'd like a live online demonstration of Attune and the opportunity to ask questions,
please `contact AttuneOps <https://attuneops.io/request-a-demo/>`_.

----

Continue to learn about Attune's :ref:`Design, Plan, and Run Framework<overview>`

or jump straight into a tutorial: :ref:`Linux or macOS<tutorial_linux>`

.. toctree::
    :titlesonly:

    overview
    tutorials/index
