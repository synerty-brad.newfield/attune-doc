.. _overview:

####################################
Design, Plan, and Run Framework
####################################

Attune is divided into 3 workspaces:

* Design,
* Plan, and
* Run.

**Design**

The :ref:`Design Workspace <workspace_design>` is where
you create your automated and reusable building blocks.

**Plan**

The :ref:`Plan Workspace <workspace_plan>` is where
you’ll plan a job to run a blueprint and which values are connected to the
parameters in the blueprint. A single blueprint can be used for many jobs
with different values.

Schedules can be planned to orchestrate multiple Jobs.

**Run**

The :ref:`Run Workspace <workspace_run>` is where you
control your Jobs, Debug your jobs and view
your historical logs.

Take a moment to view our quick 2-minute overview of the Design, Plan, and Run Framework:

.. raw:: html

    <embed>
        <iframe height="500pt" width="700pt"
        src="https://www.youtube-nocookie.com/embed/n3rh_cl2mUk"
        title="YouTube video player" frameborder="0" allow="accelerometer;
        autoplay; clipboard-write; encrypted-media; gyroscope;
        picture-in-picture" allowfullscreen></iframe>
    </embed>

:Video: Intro to Attune in 2 Minutes
