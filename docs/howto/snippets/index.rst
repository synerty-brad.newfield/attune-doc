.. _snippets:

===================
Script Snippets
===================

.. toctree::
    :maxdepth: 2
    :caption: Contents:

    powershell/index
    bash/index
    sql/index
    mako/index
