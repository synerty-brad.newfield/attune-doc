.. _dynamicOracleBash:

=================================================
How To Create Dynamic SQL Script from Bash
=================================================

.. code-block::
    :linenos:

    sqlplus splex/$Splex_PW > $Action_File << EOF
    set HEADING OFF
    set ECHO OFF
    set FEEDBACK OFF
    set pages 0
    set lines 200
    select 'alter trigger '||owner||'.'||trigger_name||' $action; from dba_triggers where trigger_name like '%SPX%';
    select 'alter table '||table_owner||'.'||table_name||' modify t_updated_by_spx date default sysdate;' from dba_triggers where trigger_name like '%SPX%' ;
    @Action_File
    exit
    EOF

    rm $Action_File
