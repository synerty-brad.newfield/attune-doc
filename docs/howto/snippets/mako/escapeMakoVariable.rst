.. _escapeMakoVariable:

=================================
How To Escape a Mako Variable
=================================

To stop Attune from trying to substitute a variable, escape it by enclosing it inside Mako raw text tags. In this
example, line 2 will be printed exactly as it is written.

.. code-block::
    :linenos:

    <%text>
    user=cyrus argv=/usr/lib/cyrus-imapd/deliver -e -r ${sender} -m ${extension} ${user}
    </%text>

