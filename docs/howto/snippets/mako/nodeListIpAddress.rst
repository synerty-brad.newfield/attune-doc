.. _nodeListIpAddress:

================================================
How To Render a Node List value IP addresses
================================================

This code snippet renders the IP addresses of nodes in a node list on
a single line separated by spaces.

The makoParameter name is appServers.

.. code-block::
    :linenos:

    ${appServers.serverIps.replace(",", " ")}
