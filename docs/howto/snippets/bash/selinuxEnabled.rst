.. _selinuxEnabled:

===========================================
How To Check Whether SELinux is Enabled
===========================================

.. code-block:: bash
    :linenos:

    if sestatus | grep -i "SELinux status:" | grep -q "enabled"
    then
        echo "SElinux is enabled"
    else
        echo "SElinux is disabled"
    fi
