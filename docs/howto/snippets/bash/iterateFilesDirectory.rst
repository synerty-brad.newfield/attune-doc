.. _iterateFilesDirectory:

============================================
How To Iterate Over Files in a Directory
============================================

:code:`ls` is not the command that expands globs to filenames, bash does that.
In the following snippet, bash expands :code:`/dev/{s,v}*1` and expands it to :code:`/dev/sda1` which is what the for loop is
then passed.

.. code-block:: bash
    :linenos:

    # This only works if there are no spaces in the file names.
    for f in /dev/{s,v}*1
    do
        echo "$f"
    done

