.. _grepStdErr:

=======================================
How To Grep the Standard Error
=======================================

The following example:

#. Executes perl Makefile.PL and saves the standard error of this command to a variable.
#. Echoes the standard error so that it appears normal in the Attune output.
#. Searches the standard error for a specific message and fails the step with an exit code of 34 if found.

The standard output is printed as normal.

.. code-block:: bash
    :linenos:

    { stderr="$( { perl Makefile.PL; } 2>&1 1>&3 3>&- )"; } 3>&1
    IFS=$'\n' echo "${stderr}" 1>&2
    grep -q "Warning: prerequisite" <<< ${stderr} && exit 34
