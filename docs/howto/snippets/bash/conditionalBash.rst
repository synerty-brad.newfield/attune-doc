.. _conditionalBash:

===============================
Conditional Bash Step in Attune
===============================

The following code can be used to create a conditional step in attune, that will only execute if an Attune variable
is literally :code:`true`.

.. code-block:: bash
    :linenos:

    if {attuneCheckVariable}
    then
        echo "Check passed, will execute code"
        # code to execute here
    else
        echo "Check failed, won't execute code"
    fi
