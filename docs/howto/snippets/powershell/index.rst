.. _snippets_powershell:

==============================
Powershell Script Snippets
==============================

.. toctree::
    :maxdepth: 1
    :caption: Contents:

    startAttuneFromLocalUser
    restartWinSnippet
    shutdownWinSnippet
    fqdnWinSnippet
    exitCodeWinSnippet
    findOvirtDriversWinSnippet
    setTimezoneServerWinSnippet
