.. _findOvirtDrivers:

====================================================
How To Search for Installed oVirt VIRTIO Drivers
====================================================

This command will list the oVirt VIRTIO Drivers installed on a machine:

.. code-block:: powershell
    :linenos:

    PS C:\Users\Administrator> driverquery | Select-String -Pattern "VirtIO"


