.. _faq_using_attune:

=================================
Attune Frequently Asked Questions
=================================

**Why can't I import or export large blueprints?**

Several reasons may cause large blueprint files to fail to import or export from the Attune Web App.

#. Check that the :code:`/tmp` and :code:`/home` directories have enough storage with :code:`df -h`

#. Check that the browser is not enforcing a download size limit by trying another browser.

#. Ensure that it is not a firewall preventing the download by connecting to the Attune server from the same subnet.


