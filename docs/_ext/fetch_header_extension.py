import subprocess

from sphinx.util import logging

logger = logging.getLogger(__name__)


def fetch_header(app):
    try:
        subprocess.check_call(["python", "fetch_header.py"])
        logger.info("fetch_header.py executed successfully.")
    except subprocess.CalledProcessError as e:
        logger.error(f"Error executing fetch_header.py: {e}")


def setup(app):
    app.connect("builder-inited", fetch_header)
