import os

import requests
from bs4 import BeautifulSoup

HEADER_FILE = "_themes/sphinx_rtd_theme/partials/header.html"


def fetch_header():
    # Fetch header from URL
    url = "https://attuneops.io"
    response = requests.get(url)
    soup = BeautifulSoup(response.text, "html.parser")
    header = soup.find("header")

    # Highlight the Docs item
    element = header.find("span", class_="link", text="Docs")
    element.parent.parent["class"] += ["current-menu-item", "current_page_item"]

    header_html = str(header)
    return header_html


def update_header_file(header_html):
    # Check if header content has changed
    if os.path.exists(HEADER_FILE):
        with open(HEADER_FILE, "r") as file:
            existing_content = file.read()
        if existing_content == header_html:
            # No change, return without updating
            return

    # Write new header content to file
    with open(HEADER_FILE, "w") as file:
        file.write(header_html)


if __name__ == "__main__":
    header_html = fetch_header()
    update_header_file(header_html)
