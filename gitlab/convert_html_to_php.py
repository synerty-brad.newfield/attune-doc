import argparse
import os
from textwrap import dedent


def convert_html_to_php(directory, auth_string, tracker_site):
    tracking_code = dedent(
        f"""
        // START TRACKING
        if (isset($_SERVER['HTTP_REFERER'])) {{
            $referer = $_SERVER['HTTP_REFERER'];
        }} else {{
            $referer = NULL;
        }}
        $data = array(
            'ip' => $_SERVER['REMOTE_ADDR'],
            'referer' => $referer,
            'date' => date('c'),
            'url' => "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'],
            'auth' => '{auth_string}'
        );
        $url = '{tracker_site}';
        $payload = json_encode($data);
        
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, TRUE);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'User-Agent: attuneops website'
        ));
        
        curl_setopt($curl, CURLOPT_TIMEOUT, 1);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, false);
        curl_setopt($curl, CURLOPT_FORBID_REUSE, true);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 1);
        curl_setopt($curl, CURLOPT_DNS_CACHE_TIMEOUT, 3600); 
        
        curl_setopt($curl, CURLOPT_FRESH_CONNECT, true);
        
        curl_exec($curl);   
        curl_close($curl);  

        // END TRACKING
        """
    )

    for root, dirs, files in os.walk(directory):
        for file in files:
            if file.endswith(".html"):
                html_file = os.path.join(root, file)
                php_file = os.path.join(root, file.replace(".html", ".php"))
                with open(html_file, "r") as f:
                    html_content = f.read()
                with open(php_file, "w") as f:
                    f.write(
                        dedent(
                            f"""<?php
                        
                            // Include tracking code
                            {tracking_code}
                            
                            // Output HTML content
                            ?>
                            {html_content}
                            """
                        )
                    )
                print(f"Converted {html_file} to {php_file}")

    with open(os.path.join(directory, ".htaccess"), "w") as htaccess_file:
        htaccess_file.write(
            dedent(
                r"""
            RewriteEngine On
            RewriteBase /docs
            RewriteRule ^$ index.html [L]
            RewriteCond %{REQUEST_FILENAME}.php -f
            RewriteRule ^(.+)\.html$ \$1.php [L]
            
            # Ensure PHP is handled correctly
            AddHandler application/x-httpd-php .php
            
            # Force PHP parsing in this directory
            <FilesMatch \.php$>
                SetHandler application/x-httpd-php
            </FilesMatch>
            
            # Enable PHP execution
            php_flag engine on
            
            # Prevent directory listing
            Options -Indexes
            
            # Set the default character set
            AddDefaultCharset UTF-8
            """
            )
        )


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Convert HTML files to PHP and add tracking code."
    )
    parser.add_argument(
        "directory", help="Directory containing HTML files to convert"
    )
    parser.add_argument(
        "auth_string", help="Authentication string for tracking"
    )
    parser.add_argument("tracker_site", help="HTTPS url to post JSON")
    args = parser.parse_args()

    convert_html_to_php(args.directory, args.auth_string, args.tracker_site)
