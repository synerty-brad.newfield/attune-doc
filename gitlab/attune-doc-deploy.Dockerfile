FROM almalinux:9.4

WORKDIR /root

RUN dnf update -y

RUN dnf -y install dnf-plugins-core
RUN dnf config-manager --set-enabled crb
RUN dnf -y install epel-release

RUN dnf -y --allowerasing install curl jq

RUN dnf -y install openssh-clients

RUN dnf -y install python3

