FROM python:3.12.4

COPY ../requirements.txt .

RUN pip install -r requirements.txt
