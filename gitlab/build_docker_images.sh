#!/bin/bash


set -o errexit
set -o nounset

cp ../requirements.txt .

IMAGE_NAMES=""
IMAGE_NAMES="${IMAGE_NAMES} attune-doc-build:master "
IMAGE_NAMES="${IMAGE_NAMES} attune-doc-deploy:master "

for IMAGE_NAME_ in ${IMAGE_NAMES}; do

    for IMAGE_NAME in ${IMAGE_NAME_} ${IMAGE_NAME_/latest/master}
    do
        echo "Building |${IMAGE_NAME_}|"
        docker build -t ${IMAGE_NAME} \
            -f $(cut -d':' -f 1 <<< "$IMAGE_NAME").Dockerfile .

        docker tag ${IMAGE_NAME} nexus.synerty.com:5001/${IMAGE_NAME}
        docker push nexus.synerty.com:5001/${IMAGE_NAME}
    done

done


rm requirements.txt
