#!/usr/bin/env bash

set -o nounset
set -o errexit


path="$(cd $(dirname $0) && pwd)"
echo "Detected script path as $path"

ARGS="--host 0.0.0.0"
ARGS="$ARGS --port 8020"
ARGS="$ARGS . ../dist"

echo "Running sphinx-autobuild with args :"
echo "$ARGS"

# Run the command
(cd $path/docs && sphinx-autobuild $ARGS)
