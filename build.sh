#!/bin/bash
set -e

# Build the documentation
(cd docs && sphinx-build -b html . ../dist)
