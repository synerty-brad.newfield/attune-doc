.. _setup_winrm_via_ad:

==================
Setup WinRM Via AD
==================

Setting up WinRM via Active Directory:

Objective
---------

This procedure provides instructions to automatically enable WinRM with HTTPS via Active Directory group policies.

Attune uses WinRM to execute commands on windows desktops and servers. WinRM, combined with improvements in PowerShell Cmdlets is Microsofts emerging solution for scriptable administration of windows servers.

.. note:: If you don't have a domain and the target computers joined to the domain, then this procedure isn't for you.

.. note:: This setup is straight forward with defaults, your corporate environment may require alterations to the procedure.

Setup :

#.  Windows 2012 R2 Server, with Active Directory Domain Services configured.

#.  Target servers are joined to the domain.

Procedure
---------

The following procedure all performed via a Remote Desktop session to the domain server.

Adding Certificate Server Role
``````````````````````````````
 ::

        # SCP ISO to Attune server

        F=17763.737.190906-2324.rs5_release_svc_refresh_SERVER_EVAL_x64FRE_en-us_1.iso

        sudo mount -o loop $F /mnt
        sudo cp -pr /mnt win2019
        sudo umount /mnt
        sudo chown -R attune:attune win2019
        sudo chmod -R u+rw win2019

        OFFSET=`isoinfo -d -i $F | grep Bootoff | cut  -c21-`
        dd if=$F of=win2019/boot.img bs=2048 count=8 skip=$OFFSET

        cd win2019/
        tar cvf ../win2019.tar *

        # SCP win2019.tar from Attune Server
        # Drop onto Archive

Complete
--------

This procedure is now complete, You can Create new Windows Server values in Attune and set the WinRM specification to "WinRM 2.0 HTTPS"
